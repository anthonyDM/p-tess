<?php
/**
 *
 * Tess smart ORM library for php 5.3 or newer
 *
 * Tess is written by Anthony De Meulemeester under the MIT-licence
 *
 * Happy php-ing
 */
require_once 'interface/dbDriver.php';

class Database implements dbDriver{

    private $pdo;
    private $freeze;
    private $table;
    private $primaryKey = 'id';

    public function __construct($table_name)
    {
        $this->table = $table_name;
    }

    public function setup()
    {
        $cfg = parse_ini_file('config/config.ini');
        $this->pdo = new PDO('mysql:host=' . $cfg['host']. ';dbname=' . $cfg['dbname'], $cfg['user'], $cfg['password']);
    }

    public function getTableName()
    {
        return $this->table;
    }

    public function setPrimaryKey($primaryKey)
    {
        $this->primaryKey = $primaryKey;
    }

    public function getPrimaryKey()
    {
        return $this->primaryKey;
    }

    public function load($row, $table, $where, $sort = NULL)
    {
        $q = "select $row from $table";

        if($where != NULL)
        {
            $q .= ' where ' . $where;
        }

        $sth = $this->pdo->prepare($q);
        $sth->execute();

        return $sth->fetchObject();
    }

    public function getAll($rows, $table, $sort = NULL)
    {
        $q = "select $rows from $table";

        if($sort != NULL)
        {
            $q.= ' order by ' . $sort;
        }

        $sth = $this->pdo->prepare($q);
        $sth->execute();

        return $sth->fetchall(PDO::FETCH_OBJ);
    }

    public function store($table, $values = array())
    {

        if($this->freeze != 1)
        {

            $sth = $this->pdo->prepare("CREATE TABLE IF NOT EXISTS $table (`id` int(11) NOT NULL AUTO_INCREMENT, PRIMARY KEY (`id`))");
            $sth->execute();

            foreach($values as $key => $value)
            {
                switch($value)
                {
                    case !is_numeric($value):

                        $vals[] = 'ADD ' . $key . ' VARCHAR(250) NOT NULL' ;
                        break;

                    case is_numeric($value):

                        $vals[] = 'ADD ' . $key . ' int(100) NOT NULL' ;
                        break;
                }
            }

            $vals = implode(', ', $vals);

            $sth = $this->pdo->prepare("ALTER TABLE $table $vals");
            $sth->execute();

            $keys = implode(', ', array_keys($values));
            $vals = ':' . implode(', :', array_keys($values));

            $sth = $this->pdo->prepare("insert into $table ($keys) values ($vals)");

            foreach($values as $key => $value)
            {
                $sth->bindvalue(":$key", $value);
            }

            $sth->execute();

            return $sth->rowCount();

        } else {

            $keys = implode(', ', array_keys($values));
            $vals = ':' . implode(', :', array_keys($values));

            $sth = $this->pdo->prepare("insert into $table ($keys) values ($vals)");

            foreach($values as $key => $value)
            {
                $sth->bindvalue(":$key", $value);
            }

            $sth->execute();

            return $sth->rowCount();
        }
    }

    public function update($table, $values = array(), $where)
    {
        $keyVals = null;

        foreach($values as $key => $value)
        {
            $keyVals .= $key.' = :'.$key.', ';
        }

        $keyVals = rtrim($keyVals, ", ");
        $sth = $this->pdo->prepare("update $table set $keyVals where $where");

        foreach($values as $key => $value)
        {
            $sth->bindvalue(":$key", $value);
        }

        $sth->execute();

        return $sth->rowCount();
    }

    public function trash($table, $where)
    {
        $sth = $this->pdo->prepare("delete from $table where $where");
        $sth->execute();

        return $sth->rowCount();
    }

    public function freeze($int)
    {
        $this->freeze = $int;
    }
}