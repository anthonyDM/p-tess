<?php
require_once 'Database.php';
require_once 'interface/entityDriver.php';

class Entity implements \entitydriver
{
    private $db;
    private $id;

    public function __construct($table_name)
    {
        $this->db = new \Database($table_name);
        $this->db->setup();
    }

    public function __call($param, $handle)
    {
        $key = substr($param, 3);
        $key = strtolower($key);

        if (!array_key_exists($key, $this))
        {
            return $key . ' is not set in ' . $this;
        }

        return $this->$key;
    }

    public function toArray()
    {
        $handle = get_object_vars($this);
        unset($handle['db']);

        return $handle;
    }

    public function save()
    {
        $this->db->store($this->db->getTableName(), $this->toArray());
    }

    public function load($param)
    {
        $entity = $this->db->load('*', $this->db->getTableName(), $this->db->getPrimaryKey() . ' = ' . $param );
        $this->id = $entity->id;

        return $entity;
    }

    public function loadAll($rows)
    {
        $all = $this->db->getAll($rows, $this->db->getTableName());

        return $all;
    }

    public function update()
    {
        $this->db->update($this->db->getTableName(), $this->toArray(), $this->db->getPrimaryKey() . ' = ' . $this->id);
    }

    public function delete()
    {
        $this->db->trash($this->db->getTableName(), $this->db->getPrimaryKey() . ' = ' . $this->id);
    }
}
