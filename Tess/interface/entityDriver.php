<?php

interface entitydriver
{
    public function __call($param, $handle);

    public function toArray();

    public function save();

    public function load($param);

    public function loadAll($rows);

    public function update();

    public function delete();

}