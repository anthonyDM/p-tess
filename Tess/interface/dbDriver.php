<?php

interface dbDriver{

/**
* Initialise tess object with db information.
* @param your database specified information.
*/
public function setup();

/**
* Runs a query and fetches specified column.
* Returns std class as object of the column.
*/
public function load($row, $table, $where);

/**
* Runs a query and fetches all data.
* Returns std class in associative array.
*/
public function getAll($rows, $table, $sort = NULL);

/**
* Runs a query and inserts data
* Returns affected rows
*/
public function store($table, $values = array());

/**
* Runs a query and updates the specified column.
* Returns affected rows
*/
public function update($table, $values = array(), $where);

/**
* Runs a query and deletes the specified column.
* Returns affected rows
*/
public function trash($table, $where);

/**
* Freezes auto create of tables in database
* @param 1 or 0
*/
public function freeze($int);

public function getTableName();

public function setPrimaryKey($primaryKey);

public function getPrimaryKey();

}